FTS3
====
FTS3 is the service responsible for globally distributing the majority of the LHC data across the WLCG infrastructure. Is a low level data movement service, responsible for reliable bulk transfer of files from one site to another while allowing participating sites to control the network resource usage

## References
* Web page: http://fts3-service.web.cern.ch/
* Documentation: http://fts3-docs.web.cern.ch/fts3-docs/docs/cli.html
* Developers guide: http://fts3-docs.web.cern.ch/fts3-docs/docs/developers.html
* Ticket handling in [JIRA](https://its.cern.ch/jira/browse/FTS/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel)
* Continuous integration in [Jenkins](https://jenkins.cern.ch/fts-dmc/)
* Monitored by the [Dashboard](http://dashb-fts-transfers.cern.ch/ui/)
* For help, contact fts-support@cern.ch, or fts-devel@cern.ch
