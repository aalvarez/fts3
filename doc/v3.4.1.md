Release Notes - FTS - Version fts 3.4.1
=======================================

## Bug
* [[FTS-436]](https://its.cern.ch/jira/browse/FTS-436) - fdopen may fail & race condition in generateOauthConfigFile
* [[FTS-438]](https://its.cern.ch/jira/browse/FTS-438) - Job metadata not being submitted properly by the CLI to the REST API
* [[FTS-439]](https://its.cern.ch/jira/browse/FTS-439) - Strict copy not honored when using REST on the CLI

