/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2010-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ws-ifce/gsoap/gsoap_stubs.h"

#include "ConfigurationHandler.h"
#include "Blacklister.h"
#include "../AuthorizationManager.h"
#include "../CGsiAdapter.h"

#include "db/generic/SingleDbInstance.h"

#include "common/Exceptions.h"
#include "common/Logger.h"

#include "DrainMode.h"

#include <vector>
#include <string>
#include <set>
#include <exception>
#include <utility>

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace db;
using namespace fts3::common;
using namespace boost;
using namespace boost::algorithm;
using namespace fts3::ws;


int fts3::implcfg__setConfiguration(soap* soap, config__Configuration *_configuration, struct implcfg__setConfigurationResponse &response)
{
    (void) response;

    vector<string>& cfgs = _configuration->cfg;
    vector<string>::iterator it;

    try
        {
            CGsiAdapter cgsi(soap);
            string dn = cgsi.getClientDn();

            ConfigurationHandler handler (dn);
            for(it = cfgs.begin(); it < cfgs.end(); it++)
                {
                    handler.parse(*it);
                    // authorize each configuration operation separately for each se/se group
                    AuthorizationManager::instance().authorize(
                        soap,
                        AuthorizationManager::CONFIG,
                        AuthorizationManager::dummy
                    );
                    handler.add();

                    // Audit
                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, *it, "set-config");
                }
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(soap, ex.what(), "ConfigurationException");
            return SOAP_FAULT;
        }
    catch(...)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An unknonw exception has been caught: setConfiguration"  << commit;
            soap_receiver_fault(soap, "setConfiguration", "ConfigurationException");
            return SOAP_FAULT;

        }
    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__getConfiguration(soap* soap, string all, string name, string source, string destination, implcfg__getConfigurationResponse & response)
{
    response.configuration = soap_new_config__Configuration(soap, -1);
    try
        {
            CGsiAdapter cgsi(soap);
            string dn = cgsi.getClientDn();

            bool allcfgs = source.empty() && destination.empty();
            bool standalone = !source.empty() && destination.empty();
            bool pair = !source.empty() && !destination.empty();
            bool symbolic_name = !name.empty();

            ConfigurationHandler handler (dn);
            if (allcfgs)
                {
                    response.configuration->cfg = handler.get();
                }
            else if (standalone)
                {
                    // the user is querying for activity share configuration
                    if (all == "vo")
                        {
                            response.configuration->cfg.push_back(handler.getVo(source));
                        }
                    // the user is querying for all configuration regarding given SE
                    else if (all == "all")
                        {
                            response.configuration->cfg = handler.getAll(source);
                        }
                    // standard standalone configuration
                    else
                        {
                            response.configuration->cfg.push_back(handler.get(source));
                        }
                }
            else if (pair)
                {
                    response.configuration->cfg.push_back(handler.getPair(source, destination));
                }
            else if (symbolic_name)
                {
                    response.configuration->cfg.push_back(handler.getPair(name));
                }
            else
                {
                    throw UserError("Wrongly specified parameters, either both the source and destination have to be specified or the configuration name!");
                }

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(soap, ex.what(), "ConfigurationException");
            return SOAP_FAULT;
        }
    catch(...)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An unknown exception has been caught: getConfiguration"  << commit;
            soap_receiver_fault(soap, "getConfiguration", "ConfigurationException");
            return SOAP_FAULT;

        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__delConfiguration(soap* soap, config__Configuration *_configuration, struct implcfg__delConfigurationResponse &_param_11)
{
    (void) _param_11;

    vector<string>& cfgs = _configuration->cfg;
    vector<string>::iterator it;

    try
        {
            CGsiAdapter cgsi(soap);
            string dn = cgsi.getClientDn();

            ConfigurationHandler handler (dn);
            for(it = cfgs.begin(); it < cfgs.end(); it++)
                {
                    handler.parse(*it);
                    // authorize each configuration operation separately for each se/se group
                    AuthorizationManager::instance().authorize(
                        soap,
                        AuthorizationManager::CONFIG,
                        AuthorizationManager::dummy
                    );
                    handler.del();

                    // Audit
                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, *it, "del-config");
                }

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(soap, ex.what(), "ConfigurationException");
            return SOAP_FAULT;
        }
    catch(...)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An unknown exception has been caught: delConfiguration" << commit;
            soap_receiver_fault(soap, "delConfiguration", "ConfigurationException");
            return SOAP_FAULT;

        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__doDrain(soap* ctx, bool drain, struct implcfg__doDrainResponse &_param_13)
{
    (void) _param_13;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user dn
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // prepare the command for audit
            stringstream cmd;
            cmd << "fts-config-set --drain " << (drain ? "on" : "off");

            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "Turning " << (drain ? "on" : "off") << " the drain mode" << commit;
            fts3::server::DrainMode::instance() = drain;
            // audit the operation
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "drain");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the drain mode cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__showUserDn(soap* ctx, bool show, implcfg__showUserDnResponse& resp)
{
    (void) resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user dn
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // prepare the command for audit
            stringstream cmd;
            cmd << "fts-config-set --drain " << (show ? "on" : "off");

            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "Turning " << (show ? "on" : "off") << " the show-user-dn mode" << commit;

            DBSingleton::instance().getDBObjectInstance()->setShowUserDn(show);

            // audit the operation
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "show-user-dn");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the drain mode cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__setRetry(soap* ctx, std::string vo, int retry, implcfg__setRetryResponse& _resp)
{
    (void) _resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user dn
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // prepare the command for audit
            stringstream cmd;
            cmd << "fts-config-set --retry " << vo << " " << retry;

            // audit the operation
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "retry");

            // set the number of retries
            DBSingleton::instance().getDBObjectInstance()->setRetry(retry, vo);

            // log it
            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "User: " << dn << " had set the retry value to " << retry << " for VO " << vo << commit;

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the number of retries cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__setOptimizerMode(soap* ctx, int optimizer_mode, implcfg__setOptimizerModeResponse &resp)
{
    (void) resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user dn
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // prepare the command for audit
            stringstream cmd;
            cmd << "fts-config-set --optimizer-mode " << optimizer_mode;

            // audit the operation
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "optimizer mode");

            // set the number of retries
            DBSingleton::instance().getDBObjectInstance()->setOptimizerMode(optimizer_mode);

            // log it
            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "User: " << dn << " had set the optmizer mode to " << optimizer_mode << commit;

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, setOptimizerMode cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

/* ---------------------------------------------------------------------- */

int fts3::implcfg__setQueueTimeout(soap* ctx, unsigned int timeout, implcfg__setQueueTimeoutResponse& resp)
{
    (void) resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user dn
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // prepare the command for audit
            stringstream cmd;
            cmd << "fts-config-set --queue-timeout " << timeout;

            // audit the operation
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "queue-timeout");

            // set the number of retries
            DBSingleton::instance().getDBObjectInstance()->setMaxTimeInQueue(timeout);

            // log it
            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "User: " << dn << " had set the maximum timeout in the queue to " << timeout << commit;

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, setQueueTimeout cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setBringOnline(soap* ctx, config__BringOnline *bring_online, implcfg__setBringOnlineResponse &resp)
{
    (void) resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            // get user VO and DN
            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            vector<config__BringOnlineTriplet*>& v = bring_online->boElem;
            vector<config__BringOnlineTriplet*>::iterator it;

            for (it = v.begin(); it != v.end(); it++)
                {

                    config__BringOnlineTriplet* triplet = *it;

                    DBSingleton::instance().getDBObjectInstance()->setMaxStageOp(
                        triplet->se,
                        triplet->vo.empty() ? vo : triplet->vo,
                        triplet->value,
                        triplet->operation
                    );

                    // Audit
                    stringstream cmd;
                    cmd << "fts-config-set --" << triplet->operation
                        << " " << triplet->se
                        << " " << triplet->value;
                    if (triplet->vo.empty())
                        cmd << " " << vo;
                    else
                        cmd << " " << triplet->vo;

                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "max-ops");

                    // log it
                    FTS3_COMMON_LOGGER_NEWLOG (INFO)
                            << "User: "
                            << dn
                            << " had set the maximum number of concurrent " << triplet->operation << " operations for se : "
                            << triplet->se << " and vo : " << vo
                            << " to "
                            << triplet->value
                            << commit;
                }

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the bring online limit"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setBandwidthLimit(soap* ctx, fts3::config__BandwidthLimit* limit, fts3::implcfg__setBandwidthLimitResponse& resp)
{
    (void) resp;

    try
        {
            // authorize
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            vector<config__BandwidthLimitPair*>::iterator it;
            for (it = limit->blElem.begin(); it != limit->blElem.end(); ++it)
                {
                    config__BandwidthLimitPair* pair = *it;

                    if (!pair->source.empty() && !pair->dest.empty())
                        throw UserError("Only source OR destination can be specified");
                    if (pair->source.empty() && pair->dest.empty())
                        throw UserError("Need to specify source OR destination");

                    DBSingleton::instance().getDBObjectInstance()->setBandwidthLimit(
                        pair->source, pair->dest, pair->limit);

                    if (pair->limit >= 0)
                        {
                            FTS3_COMMON_LOGGER_NEWLOG (INFO)
                                    << "User: "
                                    << dn
                                    << " had set the maximum bandwidth of "
                                    << pair->source << pair->dest
                                    << " to "
                                    << pair->limit << "MB/s"
                                    << commit;
                        }
                    else
                        {
                            FTS3_COMMON_LOGGER_NEWLOG (INFO)
                                    << "User: "
                                    << dn
                                    << " had reset the maximum bandwidth of "
                                    << pair->source << pair->dest
                                    << commit;
                        }
                    // prepare the command for audit
                    stringstream cmd;

                    cmd << dn;
                    cmd << " had set the maximum bandwidth of ";
                    cmd << pair->source << pair->dest;
                    cmd << " to ";
                    cmd << pair->limit << "MB/s";
                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "max-bandwidth");
                }
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, setBandwidthLimit cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__getBandwidthLimit(soap* ctx, fts3::implcfg__getBandwidthLimitResponse& resp)
{
    try
        {
            resp.limit = DBSingleton::instance().getDBObjectInstance()->getBandwidthLimit();
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, getBandwidthLimit cannot be set"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setSeProtocol(soap* ctx, string protocol, string se, string state, implcfg__setSeProtocolResponse &resp)
{
    (void) resp;

    try
        {
            if (state != "on" && state != "off") throw UserError("the protocol may be either set to 'on' or 'off'");

            // Authorise operation
            AuthorizationManager::instance().authorize(ctx, AuthorizationManager::CONFIG, AuthorizationManager::dummy);

            DBSingleton::instance().getDBObjectInstance()->setSeProtocol(protocol, se, state);

            // get user DN
            CGsiAdapter cgsi(ctx);
            string dn = cgsi.getClientDn();

            // audit the operation
            string cmd = "fts3-config-set --protocol " + protocol + " " + se + " " + state;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd, "protocol");
        }
    catch(BaseException& ex)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationnException");
            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__maxSrcSeActive(soap* ctx, string se, int active, implcfg__maxSrcSeActiveResponse& resp)
{
    (void) resp;

    try
        {
            // Authorise
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setSourceMaxActive(se, active);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn;
            cmd << " had set the maximum number of active for source SE: ";
            cmd << se;
            cmd << " to ";
            cmd << active;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "max-se-source-active");

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the maxSrcSeActive failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__maxDstSeActive(soap* ctx, string se, int active, implcfg__maxDstSeActiveResponse& resp)
{
    (void) resp;

    try
        {
            // Authorise
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setDestMaxActive(se, active);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn;
            cmd << " had set the maximum number of active for destination SE: ";
            cmd << se;
            cmd << " to ";
            cmd << active;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "max-se-dest-active");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the maxDstSeActive failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__fixActivePerPair(soap* ctx, string source, string destination, int active, implcfg__fixActivePerPairResponse& resp)
{
    (void) resp;

    try
        {
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setFixActive(source, destination, active);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn;
            cmd << " had set the fixed number of active between ";
            cmd << source;
            cmd << " and ";
            cmd << destination;
            cmd << " to ";
            cmd << active;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "fix-active-per-pair");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the fixActivePerPair failed"  << commit;
            return SOAP_FAULT;
        }
    return SOAP_OK;
}

int fts3::implcfg__setSecPerMb(soap* ctx, int secPerMb, implcfg__setSecPerMbResponse& resp)
{
    (void) resp;

    try
        {
            // Authorise
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setSecPerMb(secPerMb);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn;
            cmd << " had set the seconds per MB to " << secPerMb;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "sec-per-mb");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the setSecPerMb failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setGlobalTimeout(soap* ctx, int timeout, implcfg__setGlobalTimeoutResponse& resp)
{
    (void) resp;

    try
        {
            // Authorise
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setGlobalTimeout(timeout);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn;
            cmd << " had set the global timeout to " << timeout;
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "global-timeout");
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the setGlobalTimeout failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setGlobalLimits(soap* ctx, fts3::config__GlobalLimits* limits, fts3::implcfg__setGlobalLimitsResponse&)
{
    try
        {
            // Authorise
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );

            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            DBSingleton::instance().getDBObjectInstance()->setGlobalLimits(limits->maxActivePerLink, limits->maxActivePerSe);

            // prepare the command for audit
            stringstream cmd;
            cmd << dn << " had set the global ";
            if (limits->maxActivePerLink)
                cmd << "active limit per link to " << *limits->maxActivePerLink;
            if (limits->maxActivePerLink && limits->maxActivePerSe)
                cmd << " and ";
            if (limits->maxActivePerSe)
                cmd <<  "active limit per se to " <<  *limits->maxActivePerSe;

            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd.str(), "global-limits");
            FTS3_COMMON_LOGGER_NEWLOG (INFO) << cmd.str() << commit;
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the setGlobalLimits failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}


int fts3::implcfg__authorizeAction(soap* ctx, fts3::config__SetAuthz* authz, fts3::implcfg__authorizeActionResponse& resp)
{
    (void) resp;

    try
        {
            AuthorizationManager::instance().authorize(
                ctx,
                AuthorizationManager::CONFIG,
                AuthorizationManager::dummy
            );
            CGsiAdapter cgsi(ctx);
            string vo = cgsi.getClientVo();
            string dn = cgsi.getClientDn();

            stringstream audit;

            DBSingleton::instance().getDBObjectInstance()->authorize(authz->add, authz->operation, authz->dn);

            if (authz->add)
                {
                    audit << "Authorize " << authz->operation << " to \"" << authz->dn << "\"";
                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, audit.str(), "authorize");
                    FTS3_COMMON_LOGGER_NEWLOG (INFO) << audit.str() << commit;
                }
            else
                {
                    audit << "Revoke " << authz->operation << " to \"" << authz->dn << "\"";
                    DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, audit.str(), "revoke");
                    FTS3_COMMON_LOGGER_NEWLOG (INFO) << audit.str() << commit;
                }
        }
    catch (BaseException& ex)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the authorizeAction failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}


int fts3::implcfg__setS3Credential(soap* ctx, std::string accessKey, std::string secretKey, std::string vo, std::string storage, implcfg__setS3CredentialResponse& resp)
{
    (void) resp;

    try
        {
            // only Root is allowed to set S3 credentials
            CGsiAdapter cgsi(ctx);
            if (!cgsi.isRoot()) throw UserError("Only root is allowed to set S3 credentials!");

            // make sure the host name is upper case
            boost::to_upper(storage);
            DBSingleton::instance().getDBObjectInstance()->setCloudStorageCredentials(
                cgsi.getClientDn(), vo, storage, accessKey, secretKey
            );
        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the setS3Credential failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::implcfg__setDropboxCredential(soap* ctx, std::string appKey, std::string appSecret, std::string apiUrl, implcfg__setDropboxCredentialResponse& resp)
{
    (void) resp;

    try
        {
            // only Root is allowed to set S3 credentials
            CGsiAdapter cgsi(ctx);
            if (!cgsi.isRoot()) throw UserError("Only root is allowed to set S3 credentials!");

            DBSingleton::instance().getDBObjectInstance()->setCloudStorage("dropbox", appKey, appSecret, apiUrl);

        }
    catch(BaseException& ex)
        {

            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "InvalidConfigurationException");

            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown, the setDropboxCredential failed"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::impltns__debugLevelSet(struct soap* soap, string source, string destination, int level, struct impltns__debugLevelSetResponse &_resp)
{
    (void) _resp;

    try
        {
            CGsiAdapter cgsi(soap);
            string dn = cgsi.getClientDn();

            FTS3_COMMON_LOGGER_NEWLOG (INFO) << "DN: " << dn << " is setting debug level to " << level
                                             << "for source: " << source << " and destination: " << destination << commit;

            AuthorizationManager::instance().authorize(soap, AuthorizationManager::CONFIG, AuthorizationManager::dummy);

            if (!source.empty())
                DBSingleton::instance().getDBObjectInstance()->setDebugLevel(source, std::string(), level);

            if (!destination.empty())
                DBSingleton::instance().getDBObjectInstance()->setDebugLevel(std::string(), destination, level);

            string cmd = "fts3-debug-set ";
            if (!source.empty()) cmd += " --source " + source;
            if (!destination.empty()) cmd += " --destination " + destination;
            cmd += " " + boost::lexical_cast<std::string>(level);
            DBSingleton::instance().getDBObjectInstance()->auditConfiguration(dn, cmd, "debug");
        }
    catch(BaseException& ex)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(soap, ex.what(), "TransferException");
            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown"  << commit;
            return SOAP_FAULT;
        }
    return SOAP_OK;
}

int fts3::impltns__blacklistSe(struct soap* ctx, std::string name, std::string vo, std::string status, int timeout, bool blk, struct impltns__blacklistSeResponse &resp)
{
    (void) resp;

    try
        {
            AuthorizationManager::instance().authorize(ctx, AuthorizationManager::CONFIG, AuthorizationManager::dummy);

            Blacklister blacklister (ctx, name, vo, status, timeout, blk);
            blacklister.executeRequest();
        }
    catch(BaseException& ex)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");
            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown "  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}

int fts3::impltns__blacklistDn(soap* ctx, string subject, bool blk, string status, int timeout, impltns__blacklistDnResponse &resp)
{
    (void) resp;

    try
        {

            AuthorizationManager::instance().authorize(ctx, AuthorizationManager::CONFIG, AuthorizationManager::dummy);

            Blacklister blacklister(ctx, subject, status, timeout, blk);
            blacklister.executeRequest();

        }
    catch(BaseException& ex)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been caught: " << ex.what() << commit;
            soap_receiver_fault(ctx, ex.what(), "TransferException");
            return SOAP_FAULT;
        }
    catch (...)
        {
            FTS3_COMMON_LOGGER_NEWLOG (ERR) << "An exception has been thrown"  << commit;
            return SOAP_FAULT;
        }

    return SOAP_OK;
}
